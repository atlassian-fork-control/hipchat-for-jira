package com.atlassian.labs.jira.dto;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;

public class NotificationDto
{
    private final String baseUrl;
    private final Issue issue;
    private final User actor;
    private final String firstStepName;
    private final String endStepName;
    private final String actionName;

    public NotificationDto(String baseUrl, Issue issue, User actor, String firstStepName, String endStepName, String actionName)
    {
        this.baseUrl = baseUrl;
        this.issue = issue;
        this.actor = actor;
        this.firstStepName = firstStepName;
        this.endStepName = endStepName;
        this.actionName = actionName;
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }

    public Issue getIssue()
    {
        return issue;
    }

    public User getActor()
    {
        return actor;
    }

    public String getFirstStepName()
    {
        return firstStepName;
    }

    public String getEndStepName()
    {
        return endStepName;
    }

    public String getActionName()
    {
        return actionName;
    }
}
