package it.com.atlassian.labs.hipchat.pageobjects;


import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class HipChatNotificationsPage extends AbstractJiraPage
{
    private final String projectKey;
    private final String url;

    @ElementBy(id = "hipchat-notifications-table")
    private PageElement notificationsTable;

    public HipChatNotificationsPage(final String projectKey)
    {
        this.projectKey = projectKey;
        this.url = "/plugins/servlet/hipchat/notifications/" + projectKey;
    }

    @Override
    public TimedCondition isAt()
    {
        return notificationsTable.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return url;
    }

    public List<HipChatNotificationsRow> getRows()
    {
        final List<PageElement> rowElements = notificationsTable.findAll(By.className("hipchat-notification-row"));
        final List<HipChatNotificationsRow> rows = new ArrayList<HipChatNotificationsRow>(rowElements.size());
        for (final PageElement rowElement : rowElements)
        {
            rows.add(new HipChatNotificationsRow(rowElement));
        }
        return rows;
    }
}
